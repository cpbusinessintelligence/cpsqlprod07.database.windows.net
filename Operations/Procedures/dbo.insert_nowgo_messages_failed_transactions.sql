SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_nowgo_messages_failed_transactions]
(
	@json NVARCHAR(MAX) = NULL,
	@exception_stack_trace NVARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    INSERT INTO [dbo].[nowgo_messages_failed_transactions]
           ([json]
           ,[exception_stack_trace]
           ,[is_processed]
           ,[created_datetime])
     VALUES
           (@json
           ,@exception_stack_trace
           ,0
           ,CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'))
		
END
GO

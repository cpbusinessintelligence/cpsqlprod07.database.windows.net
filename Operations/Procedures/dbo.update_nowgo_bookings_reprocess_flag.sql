SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[update_nowgo_bookings_reprocess_flag]
(
        @rowid bigint,@SuccessFlag smallint
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	UPDATE [dbo].[nowgo_bookings_failed_transactions] SET is_processed = @SuccessFlag,Attempt = Attempt + 1 where row_id = @rowid

	UPDATE [dbo].[nowgo_bookings_failed_transactions] SET is_processed = 1 where row_id = @rowid and attempt > 3

END
GO

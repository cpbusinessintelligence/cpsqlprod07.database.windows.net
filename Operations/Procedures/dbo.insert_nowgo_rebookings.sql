SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_nowgo_rebookings] 
(
	@booking_ref VARCHAR(30),
	@rebooking_ref VARCHAR(30) = NULL,
	@branch VARCHAR(20) = NULL,
	@driver_ref VARCHAR(20) = NULL,
	@account_number VARCHAR(20) = NULL,	
	@api_response_code VARCHAR(20) = NULL,
	@api_response_status_code VARCHAR(10) = NULL,
	@api_response_msg VARCHAR(200) = NULL,
	@api_response_booking_reference VARCHAR(30) = NULL,
	@api_response_nowgo_job_id VARCHAR(30) = NULL,
	@api_request NVARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

	INSERT INTO nowgo_rebookings 
		(booking_ref 
		,rebooking_ref 
		,branch 
		,driver_ref 
		,account_number 
		,api_response_code 
		,api_response_status_code 
		,api_response_msg 
		,api_response_booking_reference 
		,api_response_nowgo_job_id 
		,api_request 
		,created_datetime)
	VALUES
		(@booking_ref 
		,@rebooking_ref 
		,@branch 
		,@driver_ref 
		,@account_number 
		,@api_response_code 
		,@api_response_status_code 
		,@api_response_msg 
		,@api_response_booking_reference 
		,@api_response_nowgo_job_id 
		,@api_request
		,CONVERT(DATETIME,GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'));
END
GO

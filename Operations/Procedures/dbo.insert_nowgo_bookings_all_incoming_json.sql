SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[insert_nowgo_bookings_all_incoming_json]
(
        @json NVARCHAR(MAX)		
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	BEGIN

	INSERT INTO [dbo].[nowgo_bookings_all_incoming_json]
           ([json]
           ,[created_datetime])
     VALUES
           (@json
           ,CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'))
	END
END
GO

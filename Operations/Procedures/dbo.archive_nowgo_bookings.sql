SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
create proc dbo.archive_nowgo_bookings
as
begin

delete from  [dbo].[nowgo_bookings] 
output deleted.* into [dbo].[nowgo_bookings_archive]
where datediff(dd,created_datetime,getdate()) > 7

end
GO

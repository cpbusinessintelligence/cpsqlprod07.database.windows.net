SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_nowgo_bookings]
(
	@unique_id VARCHAR(50),
	@driver_ref VARCHAR(50) = NULL,
	@booking_status VARCHAR(50) = NULL,
	@booking_reference VARCHAR(50) = NULL,
	@booking_reference_to_cosmos VARCHAR(50) = NULL,
	@booking_outcome VARCHAR(50) = NULL,
	@booking_failure_reason VARCHAR(50) = NULL,	
	@api_response_code VARCHAR(50) = NULL,
	@api_response_status_code VARCHAR(50) = NULL,
	@api_response_msg VARCHAR(50) = NULL,
	@api_response_id VARCHAR(50) = NULL,
	@json_from_nowgo NVARCHAR(MAX) = NULL,
	@json_to_api NVARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    INSERT INTO [dbo].[nowgo_bookings]
		([unique_id]
		,[driver_ref]
		,[booking_status]
		,[booking_reference]
		,[booking_reference_to_cosmos]
		,[booking_outcome]
		,[booking_failure_reason]
		,[api_response_code]
		,[api_response_status_code]
		,[api_response_msg]
		,[api_response_id]
		,[json_from_nowgo]
		,[json_to_api]
		,[created_datetime])
	SELECT
		 @unique_id AS [unique_id]
		,@driver_ref AS [driver_ref]
		,@booking_status AS [booking_status]
		,@booking_reference AS [booking_reference]
		,@booking_reference_to_cosmos AS [booking_reference_to_cosmos]
		,@booking_outcome AS [booking_outcome]
		,@booking_failure_reason AS [booking_failure_reason]
		,@api_response_code AS [api_response_code]
		,@api_response_status_code AS [api_response_status_code]
		,@api_response_msg AS [api_response_msg]
		,@api_response_id AS [api_response_id]		
		,@json_from_nowgo AS [json_from_nowgo]
		,@json_to_api AS[json_to_api]
		,CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS [created_datetime];
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_nowgo_messages]
(
	@unique_id VARCHAR(50),
	@message_sender_name VARCHAR(50) = NULL,
	@message_body VARCHAR(50) = NULL,
	@message_display_name VARCHAR(50) = NULL,
	@message_driver_ext_ref VARCHAR(50) = NULL,
	@message_created_at DATETIME = NULL,
	@message_sender_role VARCHAR(50) = NULL,
	@message_source VARCHAR(50) = NULL,
	@api_response_code VARCHAR(50) = NULL,
	@api_response_status_code VARCHAR(50) = NULL,
	@api_response_msg VARCHAR(50) = NULL,
	@api_response_id VARCHAR(50) = NULL,
	@json_from_nowgo NVARCHAR(MAX) = NULL,
	@json_to_api NVARCHAR(MAX) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    INSERT INTO [dbo].[nowgo_messages]
		([unique_id]
		,[message_sender_name]
		,[message_body]
		,[message_display_name]
		,[message_driver_ext_ref]
		,[message_created_at]
		,[message_sender_role]
		,[message_source]
		,[api_response_code]
		,[api_response_status_code]
		,[api_response_msg]
		,[api_response_id]
		,[json_from_nowgo]
		,[json_to_api]
		,[created_datetime])
	SELECT
		 @unique_id AS[unique_id]
		,@message_sender_name AS [message_sender_name]
		,@message_body AS [message_body]
		,@message_display_name AS [message_display_name]
		,@message_driver_ext_ref AS [message_driver_ext_ref]
		,@message_created_at AS [message_created_at]
		,@message_sender_role AS [message_sender_role]
		,@message_source AS [message_source]
		,@api_response_code AS [api_response_code]
		,@api_response_status_code AS [api_response_status_code]
		,@api_response_msg AS [api_response_msg]
		,@api_response_id AS [api_response_id]		
		,@json_from_nowgo AS [json_from_nowgo]
		,@json_to_api AS[json_to_api]
		,CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time') AS [created_datetime];
END
GO

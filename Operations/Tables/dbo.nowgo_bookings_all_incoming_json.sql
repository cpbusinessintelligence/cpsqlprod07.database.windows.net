SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_bookings_all_incoming_json] (
		[row_id]               [int] IDENTITY(1, 1) NOT NULL,
		[json]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]     [datetime] NULL,
		CONSTRAINT [PK_nowgo_bookings_all_incoming_json]
		PRIMARY KEY
		CLUSTERED
		([row_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_bookings_all_incoming_json] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_bookings_failed_transactions] (
		[row_id]                    [int] IDENTITY(1, 1) NOT NULL,
		[json]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[exception_stack_trace]     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_processed]              [bit] NULL,
		[Attempt]                   [smallint] NULL,
		[created_datetime]          [datetime] NULL,
		CONSTRAINT [PK_nowgo_bookings_failed_transactions]
		PRIMARY KEY
		CLUSTERED
		([row_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_bookings_failed_transactions]
	ADD
	CONSTRAINT [DF_nowgo_bookings_failed_transactions_Attempt]
	DEFAULT ((0)) FOR [Attempt]
GO
ALTER TABLE [dbo].[nowgo_bookings_failed_transactions] SET (LOCK_ESCALATION = TABLE)
GO

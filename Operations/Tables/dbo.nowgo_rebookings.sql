SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_rebookings] (
		[row_id]                             [int] IDENTITY(1, 1) NOT NULL,
		[booking_ref]                        [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[rebooking_ref]                      [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[branch]                             [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driver_ref]                         [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[account_number]                     [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_code]                  [varchar](20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_status_code]           [varchar](10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_msg]                   [varchar](200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_booking_reference]     [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_nowgo_job_id]          [varchar](30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_request]                        [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]                   [datetime] NULL,
		CONSTRAINT [PK_nowgo_rebookings]
		PRIMARY KEY
		CLUSTERED
		([row_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_rebookings] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_booking_reprocessing_errorlog] (
		[RowID]                [bigint] IDENTITY(1, 1) NOT NULL,
		[ErrorData]            [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[ErrorDescription]     [varchar](8000) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[CreateDateTime]       [datetime2](7) NOT NULL,
		CONSTRAINT [PK_nowgo_booking_reprocessing_errorlog]
		PRIMARY KEY
		CLUSTERED
		([RowID])
)
GO
ALTER TABLE [dbo].[nowgo_booking_reprocessing_errorlog]
	ADD
	CONSTRAINT [DF_nowgo_booking_reprocessing_errorlog_CreateDateTime]
	DEFAULT (getdate()) FOR [CreateDateTime]
GO
ALTER TABLE [dbo].[nowgo_booking_reprocessing_errorlog] SET (LOCK_ESCALATION = TABLE)
GO

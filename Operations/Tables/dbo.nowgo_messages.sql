SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_messages] (
		[row_id]                       [int] IDENTITY(1, 1) NOT NULL,
		[unique_id]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_sender_name]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_body]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_display_name]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_driver_ext_ref]       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_created_at]           [datetime] NULL,
		[message_sender_role]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[message_source]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_code]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_status_code]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_msg]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_id]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_from_nowgo]              [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_to_api]                  [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]             [datetime] NULL,
		CONSTRAINT [PK_nowgo_messages]
		PRIMARY KEY
		CLUSTERED
		([row_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_messages] SET (LOCK_ESCALATION = TABLE)
GO

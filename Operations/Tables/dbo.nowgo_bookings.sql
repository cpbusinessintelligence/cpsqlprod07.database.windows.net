SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_bookings] (
		[row_id]                          [int] IDENTITY(1, 1) NOT NULL,
		[unique_id]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driver_ref]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_status]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_reference]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_reference_to_cosmos]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_outcome]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_failure_reason]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_code]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_status_code]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_msg]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_id]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_from_nowgo]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_to_api]                     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]                [datetime] NULL,
		CONSTRAINT [PK_nowgo_bookings]
		PRIMARY KEY
		CLUSTERED
		([row_id])
) TEXTIMAGE_ON [PRIMARY]
GO
CREATE NONCLUSTERED INDEX [nci_wi_nowgo_bookings_FBD453208F07750BAACCB156A6F619F2]
	ON [dbo].[nowgo_bookings] ([booking_status])
	INCLUDE ([booking_reference_to_cosmos], [json_from_nowgo])
	ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_bookings] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[nowgo_bookings_archive] (
		[row_id]                          [int] NULL,
		[unique_id]                       [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[driver_ref]                      [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_status]                  [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_reference]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_reference_to_cosmos]     [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_outcome]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[booking_failure_reason]          [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_code]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_status_code]        [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_msg]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[api_response_id]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_from_nowgo]                 [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json_to_api]                     [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]                [datetime] NULL
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[nowgo_bookings_archive] SET (LOCK_ESCALATION = TABLE)
GO

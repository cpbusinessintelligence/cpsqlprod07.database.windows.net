SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

-- Return all partition information for the database
CREATE VIEW [dbo].[UFN_GET_PARTITION_INFO] AS
    SELECT 
        OBJECT_NAME(i.[object_id]) AS ObjectName, 
        (SELECT [DBO].[UFN_GET_INDEX_NAME] (i.[object_id], i.index_id)) AS IndexName, 
	    p.[partition_number] AS PartitionNo, 
	    fg.[name] AS FileGroupName, 
	    p.[rows] AS TotalRows, 
	    au.[total_pages] AS TotalPages,
	    CASE pf.[boundary_value_on_right] 
            WHEN 1 THEN 'less than' 
            ELSE 'less than or equal to' 
        END AS CompareType, 
        rv.[value] AS CompareValue

    FROM 
        sys.partitions p JOIN sys.indexes i
	        ON p.[object_id] = i.[object_id] AND 
			   p.[index_id] = i.[index_id]
        JOIN sys.partition_schemes ps 
            ON ps.[data_space_id] = i.[data_space_id]
        JOIN sys.partition_functions pf 
		    ON pf.[function_id] = ps.[function_id]
        LEFT JOIN sys.partition_range_values rv    
            ON pf.[function_id] = rv.[function_id] AND 
			   p.[partition_number] = rv.[boundary_id]
        JOIN sys.destination_data_spaces dds
		    ON dds.[partition_scheme_id] = ps.[data_space_id] AND 
			   dds.[destination_id] = p.[partition_number]
 	    JOIN sys.filegroups fg 
		    ON dds.[data_space_id] = fg.[data_space_id]
	    JOIN 
	      (
	         SELECT 
			     [container_id], 
				 sum([total_pages]) as total_pages 
		     FROM 
			     sys.allocation_units
		     GROUP BY 
			     [container_id]
		  ) AS au
		    ON au.[container_id] = p.[partition_id]
    WHERE 
	    i.[index_id] < 2;
GO

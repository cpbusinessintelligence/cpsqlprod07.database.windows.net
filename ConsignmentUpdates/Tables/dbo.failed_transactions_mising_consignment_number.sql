SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[failed_transactions_mising_consignment_number] (
		[failed_transaction_id]     [int] IDENTITY(1, 1) NOT NULL,
		[label_number]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[is_processed]              [bit] NOT NULL,
		[created_datetime]          [datetime] NOT NULL,
		CONSTRAINT [PK_failed_transactions_mising_consignment_number]
		PRIMARY KEY
		CLUSTERED
		([failed_transaction_id])
) TEXTIMAGE_ON [PRIMARY]
GO
ALTER TABLE [dbo].[failed_transactions_mising_consignment_number]
	ADD
	CONSTRAINT [DF_failed_transactions_mising_consignment_number_is_processed]
	DEFAULT ((0)) FOR [is_processed]
GO
ALTER TABLE [dbo].[failed_transactions_mising_consignment_number] SET (LOCK_ESCALATION = TABLE)
GO

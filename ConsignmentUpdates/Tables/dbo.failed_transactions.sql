SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[failed_transactions] (
		[failed_transaction_id]     [int] IDENTITY(1, 1) NOT NULL,
		[json]                      [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[error_number]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[error_severity]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[error_state]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[error_line]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[error_procedure]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[error_message]             [varchar](500) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_processed]              [bit] NOT NULL,
		[created_datetime]          [datetime] NOT NULL,
		CONSTRAINT [PK_failed_transactions]
		PRIMARY KEY
		CLUSTERED
		([failed_transaction_id], [created_datetime])
)
GO
ALTER TABLE [dbo].[failed_transactions]
	ADD
	CONSTRAINT [DF_failed_transactions_is_processed]
	DEFAULT ((0)) FOR [is_processed]
GO
ALTER TABLE [dbo].[failed_transactions] SET (LOCK_ESCALATION = TABLE)
GO

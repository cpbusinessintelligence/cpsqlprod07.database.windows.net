SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[label] (
		[label_id]                  [int] IDENTITY(1, 1) NOT NULL,
		[consignment_id]            [int] NOT NULL,
		[nowgo_label_id]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[label_number]              [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[label_type]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[item_count]                [int] NULL,
		[custom_data]               [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]          [datetime] NOT NULL,
		[created_by]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[last_updated_datetime]     [datetime] NULL,
		[last_updated_by]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_label]
		PRIMARY KEY
		CLUSTERED
		([label_id], [created_datetime])
)
GO
ALTER TABLE [dbo].[label] SET (LOCK_ESCALATION = TABLE)
GO

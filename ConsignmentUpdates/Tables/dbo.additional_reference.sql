SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[additional_reference] (
		[reference_id]              [int] IDENTITY(1, 1) NOT NULL,
		[consignment_id]            [int] NOT NULL,
		[additional_reference]      [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]          [datetime] NOT NULL,
		[created_by]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[last_updated_datetime]     [datetime] NULL,
		[last_updated_by]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_additional_reference]
		PRIMARY KEY
		CLUSTERED
		([reference_id], [created_datetime])
)
GO
ALTER TABLE [dbo].[additional_reference] SET (LOCK_ESCALATION = TABLE)
GO

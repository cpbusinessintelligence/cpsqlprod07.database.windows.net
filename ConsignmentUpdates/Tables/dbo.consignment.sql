SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[consignment] (
		[consignment_id]             [int] IDENTITY(1, 1) NOT NULL,
		[nowgo_message_type]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[nowgo_message_id]           [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[consignment_number]         [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[nowgo_version]              [int] NULL,
		[number_of_items]            [int] NULL,
		[sender_address_id]          [int] NULL,
		[origin_address_id]          [int] NULL,
		[receiver_address_id]        [int] NULL,
		[destination_address_id]     [int] NULL,
		[return_address_id]          [int] NULL,
		[is_return]                  [bit] NULL,
		[custom_data]                [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]           [datetime] NOT NULL,
		[created_by]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[last_updated_datetime]      [datetime] NULL,
		[last_updated_by]            [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[json]                       [nvarchar](max) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_consignment]
		PRIMARY KEY
		CLUSTERED
		([consignment_id], [created_datetime])
)
GO
ALTER TABLE [dbo].[consignment] SET (LOCK_ESCALATION = TABLE)
GO

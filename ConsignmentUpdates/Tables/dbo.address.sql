SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[address] (
		[address_id]               [int] IDENTITY(1, 1) NOT NULL,
		[address_type]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[first_name]               [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[last_name]                [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[company_name]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[email]                    [varchar](250) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address1]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address2]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address3]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address4]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[address5]                 [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[suburb]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[state]                    [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[postcode]                 [nvarchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[latitude]                 [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[longitude]                [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[phone]                    [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[mobile]                   [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[country]                  [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[country_code]             [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[is_business]              [bit] NULL,
		[contact_name]             [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[contact_phone]            [varchar](100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		[created_datetime]         [datetime] NOT NULL,
		[created_by]               [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL,
		[last_udated_datetime]     [datetime] NULL,
		[last_updated_by]          [varchar](50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL,
		CONSTRAINT [PK_address]
		PRIMARY KEY
		CLUSTERED
		([address_id], [created_datetime])
)
GO
ALTER TABLE [dbo].[address] SET (LOCK_ESCALATION = TABLE)
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_label]
(
        @JSON NVARCHAR(MAX),
		@CREATED_BY VARCHAR(50),
		@CONSIGNMENT_ID INT,
		@CREATED_DATETIME DATETIME
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

    INSERT INTO [dbo].[label] WITH (TABLOCKX)
				([consignment_id]
				,[nowgo_label_id]
				,[label_number]
				,[label_type]
				,[item_count]
				,[custom_data]
				,[created_datetime]
				,[created_by])
	SELECT
		@CONSIGNMENT_ID AS consignment_id,
		JSON_VALUE (ARTICLES.value, '$.id') AS nowgo_label_id, 
		JSON_VALUE (IDENTIFIERS.value, '$.identifier') AS label_number, 
		JSON_VALUE (IDENTIFIERS.value, '$.relation') AS label_type,
		ISNULL(JSON_VALUE (ARTICLES.value, '$.item_count'), 1) AS item_count, 
		JSON_QUERY (ARTICLES.value, '$.custom_data') AS custom_data,
		@CREATED_DATETIME AS created_datetime,
		@CREATED_BY AS [created_by]
	FROM OPENJSON (@json, '$.message_data.consignment.articles') AS ARTICLES
	CROSS APPLY OPENJSON (ARTICLES.value, '$.identifiers') AS IDENTIFIERS
	--WHERE JSON_VALUE (IDENTIFIERS.value, '$.relation') = 'label_number'
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[insert_address]
(
    @JSON NVARCHAR(MAX),
	@ADDRESS_TYPE VARCHAR(50),
	@CREATED_BY VARCHAR(50),
	@CREATED_DATETIME DATETIME
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

	INSERT INTO [dbo].[address] WITH (TABLOCKX)
				([address_type]
				,[first_name]
				,[last_name]
				,[company_name]
				,[address1]
				,[address2]
				,[address3]
				,[address4]
				,[address5]
				,[suburb]
				,[state]
				,[postcode]
				,[latitude]
				,[longitude]
				,[is_business]
				,[contact_name]
				,[contact_phone]
				,[created_datetime]
				,[created_by]) 
	SELECT
		@ADDRESS_TYPE AS address_type,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.name') AS first_name,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.name') AS last_name,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.business_name') AS company_name,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_1'), ''), COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_2'), ''), COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_3'), ''), ''))) AS address1,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_2'), ''), NULL) AS address2,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_3'), ''), NULL) AS address3,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_4'), ''), NULL) AS address4,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.address_line_5'), ''), NULL) AS address5,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.locality') AS suburb,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.state') AS [state],
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.postal_code') AS postcode,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.latitude') AS latitude,
		JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.longitude') AS longitude,
		IIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.is_business') = 'true', 1, 0) AS [is_business],
		ISNULL(COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.contact_name'), ''), NULL), JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.name')) AS contact_name,
		COALESCE(NULLIF(JSON_VALUE(@JSON, '$.message_data.consignment.' + @ADDRESS_TYPE + '.contact_phone'), ''), NULL) AS contact_phone,
		@CREATED_DATETIME AS created_datetime,
		@CREATED_BY AS [created_by];

	RETURN SCOPE_IDENTITY();
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO


CREATE PROCEDURE [dbo].[insert_additional_reference]
(
        @JSON NVARCHAR(MAX),
		@CREATED_BY VARCHAR(50),
		@CONSIGNMENT_ID INT,
		@CREATED_DATETIME DATETIME
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;


	INSERT INTO [dbo].[additional_reference] WITH (TABLOCKX)
           ([consignment_id]
           ,[additional_reference]
           ,[created_datetime]
           ,[created_by])
    SELECT
			@CONSIGNMENT_ID AS consignment_id
           ,JSON_VALUE(IDT.value, '$.identifier') AS [additional_reference] 
           ,@CREATED_DATETIME AS created_datetime
           ,@CREATED_BY AS created_by
		   FROM OPENJSON (@JSON, '$.message_data.consignment.identifiers') IDT 
	WHERE  JSON_VALUE(IDT.value, '$.relation') = 'additional_reference'
END
GO

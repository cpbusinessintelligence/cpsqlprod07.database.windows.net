SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[sproc_Imagination_TruncatePartitions]
	@start DATETIME,
	@end DATETIME
AS
BEGIN
	SET NOCOUNT ON;

    DECLARE @partitions TABLE (Number SMALLINT)
	INSERT INTO @partitions SELECT Number FROM dbo.udf_GetDayOfTheYearRange_FromDate(@start, @end)

	DECLARE @cnt INT = 0;
	SELECT @cnt = COUNT(1) FROM @partitions

	--WHILE @cnt > 0
	--BEGIN
	--	DECLARE @partition SMALLINT
	--	SELECT TOP (1) @partition = Number FROM @partitions

	--	--TRUNCATE TABLE Imagination_Partitioned WITH (PARTITIONS(@partition))

	--	--DELETE TOP(1) FROM @partitions
	--	SELECT @cnt = COUNT(1) FROM @partitions
	--END
END
GO

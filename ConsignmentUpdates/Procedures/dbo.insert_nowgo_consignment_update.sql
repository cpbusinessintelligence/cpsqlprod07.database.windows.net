SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO



CREATE PROCEDURE [dbo].[insert_nowgo_consignment_update]
	@JSON NVARCHAR(MAX)
AS
BEGIN
	SET NOCOUNT ON;
	SET XACT_ABORT ON;

	DECLARE @RECEIVER_ID INT;
	DECLARE @SENDER_ID INT;
	DECLARE @ORIGIN_ID INT;
	DECLARE @DESTINATION_ID INT;
	DECLARE @RETURN_TO_ID INT;
	DECLARE @CONSIGNMENT_ID INT;
	DECLARE @CREATED_BY VARCHAR(50);
	DECLARE @CREATED_DATETIME DATETIME;
	DECLARE @CONSIGNMENT_NUMBER VARCHAR(50); 

	BEGIN TRAN

	BEGIN TRY

		SET @CREATED_BY = N'admin';
		SET @CREATED_DATETIME = CONVERT(DATETIME, GETDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time');
		SET @CONSIGNMENT_NUMBER = (SELECT JSON_VALUE(IDT.value, '$.identifier') AS [consignment_number] FROM OPENJSON (@JSON, '$.message_data.consignment.identifiers') IDT WHERE  JSON_VALUE(IDT.value, '$.relation') = 'consignment_number');

		IF (Select JSON_QUERY(@json,'$.message_data.consignment')) IS NOT NULL
		BEGIN

			IF (Select JSON_QUERY(@json,'$.message_data.consignment.receiver')) IS NOT NULL
				BEGIN
					EXEC @RECEIVER_ID =		
										[dbo].[insert_address] 
										@JSON = @JSON,
										@ADDRESS_TYPE = N'receiver',
										@CREATED_BY = @CREATED_BY,
										@CREATED_DATETIME = @CREATED_DATETIME;
				END

			IF (Select JSON_QUERY(@json,'$.message_data.consignment.sender')) IS NOT NULL
				BEGIN
					EXEC @SENDER_ID =		
										[dbo].[insert_address] 
										@JSON = @JSON,
										@ADDRESS_TYPE = N'sender',
										@CREATED_BY = @CREATED_BY,
										@CREATED_DATETIME = @CREATED_DATETIME;
				END

			IF (Select JSON_QUERY(@json,'$.message_data.consignment.origin')) IS NOT NULL
				BEGIN
					EXEC @ORIGIN_ID =		
										[dbo].[insert_address] 
										@JSON = @JSON,
										@ADDRESS_TYPE = N'origin',
										@CREATED_BY = @CREATED_BY,
										@CREATED_DATETIME = @CREATED_DATETIME;
				END

			IF (Select JSON_QUERY(@json,'$.message_data.consignment.destination')) IS NOT NULL
				BEGIN
					EXEC @DESTINATION_ID =	
										[dbo].[insert_address] 
										@JSON = @JSON,
										@ADDRESS_TYPE = N'destination',
										@CREATED_BY = @CREATED_BY,
										@CREATED_DATETIME = @CREATED_DATETIME;
				END

			IF (Select JSON_QUERY(@json,'$.message_data.consignment.return_to')) IS NOT NULL
				BEGIN
					EXEC @RETURN_TO_ID =	
										[dbo].[insert_address] 
										@JSON = @JSON,
										@ADDRESS_TYPE = N'return_to',
										@CREATED_BY = @CREATED_BY,
										@CREATED_DATETIME = @CREATED_DATETIME;
				END
		
			IF @CONSIGNMENT_NUMBER IS NOT NULL
				BEGIN
					EXEC @CONSIGNMENT_ID = 
										[dbo].[insert_consignment] 
										@JSON = @JSON,
										@CREATED_BY = @CREATED_BY,
										@RECEIVER_ID = @RECEIVER_ID,
										@SENDER_ID = @SENDER_ID,
										@ORIGIN_ID = @ORIGIN_ID,
										@DESTINATION_ID = @DESTINATION_ID,
										@RETURN_TO_ID = @RETURN_TO_ID,
										@CONSIGNMENT_NUMBER = @CONSIGNMENT_NUMBER,
										@CREATED_DATETIME = @CREATED_DATETIME;
					BEGIN
						EXEC [dbo].[insert_label] 
								@JSON = @JSON,
								@CREATED_BY = @CREATED_BY,
								@CONSIGNMENT_ID = @CONSIGNMENT_ID,
								@CREATED_DATETIME = @CREATED_DATETIME;			
					END


					IF (Select JSON_QUERY(@json,'$.message_data.consignment.identifiers[0]')) IS NOT NULL
						BEGIN
							EXEC [dbo].[insert_additional_reference] 
									@JSON = @JSON,
									@CREATED_BY = @CREATED_BY,
									@CONSIGNMENT_ID = @CONSIGNMENT_ID,
									@CREATED_DATETIME = @CREATED_DATETIME;			
						END
				END
			ELSE IF (Select JSON_QUERY(@json,'$.message_data.consignment.articles[0]')) IS NOT NULL AND 
					JSON_VALUE (@JSON, '$.message_data.consignment.articles[0].identifiers[0].identifier') LIKE  '[0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9][0-9]'
				BEGIN
						SET @CONSIGNMENT_NUMBER = JSON_VALUE (@JSON, '$.message_data.consignment.articles[0].identifiers[0].identifier')
						EXEC @CONSIGNMENT_ID = 
										[dbo].[insert_consignment] 
										@JSON = @JSON,
										@CREATED_BY = @CREATED_BY,
										@RECEIVER_ID = @RECEIVER_ID,
										@SENDER_ID = @SENDER_ID,
										@ORIGIN_ID = @ORIGIN_ID,
										@DESTINATION_ID = @DESTINATION_ID,
										@RETURN_TO_ID = @RETURN_TO_ID,
										@CONSIGNMENT_NUMBER = @CONSIGNMENT_NUMBER,
										@CREATED_DATETIME = @CREATED_DATETIME;

					BEGIN
						EXEC [dbo].[insert_label] 
								@JSON = @JSON,
								@CREATED_BY = @CREATED_BY,
								@CONSIGNMENT_ID = @CONSIGNMENT_ID,
								@CREATED_DATETIME = @CREATED_DATETIME;			
					END


					IF (Select JSON_QUERY(@json,'$.message_data.consignment.identifiers[0]')) IS NOT NULL
						BEGIN
							EXEC [dbo].[insert_additional_reference] 
									@JSON = @JSON,
									@CREATED_BY = @CREATED_BY,
									@CONSIGNMENT_ID = @CONSIGNMENT_ID,
									@CREATED_DATETIME = @CREATED_DATETIME;			
						END
				END
			ELSE IF @CONSIGNMENT_NUMBER IS NULL		
				BEGIN
					INSERT INTO [dbo].[failed_transactions_mising_consignment_number]
							   ([label_number]
							   ,[json]							   		   
							   ,[created_datetime])
					SELECT
							JSON_VALUE (@JSON, '$.message_data.consignment.articles[0].identifiers[0].identifier'),
							@JSON,							
							@CREATED_DATETIME;
				END
			ELSE
				BEGIN
					EXEC @CONSIGNMENT_ID = 
										[dbo].[insert_consignment] 
										@JSON = @JSON,
										@CREATED_BY = @CREATED_BY,
										@RECEIVER_ID = @RECEIVER_ID,
										@SENDER_ID = @SENDER_ID,
										@ORIGIN_ID = @ORIGIN_ID,
										@DESTINATION_ID = @DESTINATION_ID,
										@RETURN_TO_ID = @RETURN_TO_ID,
										@CONSIGNMENT_NUMBER = @CONSIGNMENT_NUMBER,
										@CREATED_DATETIME = @CREATED_DATETIME;
					BEGIN
						EXEC [dbo].[insert_label] 
								@JSON = @JSON,
								@CREATED_BY = @CREATED_BY,
								@CONSIGNMENT_ID = @CONSIGNMENT_ID,
								@CREATED_DATETIME = @CREATED_DATETIME;			
					END


					IF (Select JSON_QUERY(@json,'$.message_data.consignment.identifiers[0]')) IS NOT NULL
						BEGIN
							EXEC [dbo].[insert_additional_reference] 
									@JSON = @JSON,
									@CREATED_BY = @CREATED_BY,
									@CONSIGNMENT_ID = @CONSIGNMENT_ID,
									@CREATED_DATETIME = @CREATED_DATETIME;			
						END
				END
		END	

	END TRY

	BEGIN CATCH
		IF @@TRANCOUNT >0
		BEGIN		
			ROLLBACK TRAN;				
		END;

		INSERT INTO [failed_transactions]
			([json]
			,[error_number]
			,[error_severity]
			,[error_state]
			,[error_line]
			,[error_procedure]
			,[error_message]
			,[created_datetime])
		SELECT
			@JSON
			,ERROR_NUMBER() AS error_number  
			,ERROR_SEVERITY() AS error_severity  
			,ERROR_state() AS error_state  
			,ERROR_LINE () AS error_line  
			,ERROR_PROCEDURE() AS error_procedure  
			,ERROR_MESSAGE() AS error_message
			,@CREATED_DATETIME AS created_datetime;
			
		THROW;

	END CATCH

	IF @@TRANCOUNT >0
		BEGIN	
			COMMIT TRAN;
		END
END

GO

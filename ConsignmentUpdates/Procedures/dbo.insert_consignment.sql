SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_consignment]
(
        @JSON NVARCHAR(MAX),
		@CREATED_BY VARCHAR(50),
		@RECEIVER_ID INT,
		@SENDER_ID INT,
		@ORIGIN_ID INT,
		@DESTINATION_ID INT,
		@RETURN_TO_ID INT,
		@CONSIGNMENT_NUMBER VARCHAR(50),
		@CREATED_DATETIME DATETIME
)
AS
BEGIN
    SET NOCOUNT ON;
	SET XACT_ABORT ON;

    INSERT INTO [dbo].[consignment] WITH (TABLOCKX)
		([nowgo_message_type]
		,[nowgo_message_id]
		,[consignment_number]
		,[nowgo_version]
		,[number_of_items]
		,[sender_address_id]
		,[origin_address_id]
		,[receiver_address_id]
		,[destination_address_id]
		,[return_address_id]
		,[is_return]		
		,[custom_data]
		,[created_datetime]
		,[created_by]
		,[json])
	SELECT
		JSON_VALUE(@JSON, '$.message_type') AS [nowgo_message_type],
		JSON_VALUE(@JSON, '$.message_data.consignment.id') AS [nowgo_message_id],
		@CONSIGNMENT_NUMBER AS [consignment_number],
		JSON_VALUE(@JSON, '$.message_data.consignment.numeric_version') AS [nowgo_version],
		(SELECT COUNT(*) FROM (SELECT [key] FROM OPENJSON(@JSON,'$.message_data.consignment.articles')) AS tblArticles) AS number_of_items,
		@SENDER_ID AS [sender_address_id],
		@ORIGIN_ID AS [origin_address_id],
		@RECEIVER_ID AS [receiver_address_id],
		@DESTINATION_ID AS [destination_address_id],
		@RETURN_TO_ID AS [return_address_id],
		IIF(JSON_VALUE(@JSON, '$.message_data.consignment.is_return') = 'true', 1, 0) AS [is_return],		
		JSON_QUERY(@JSON, '$.message_data.consignment.custom_data') AS [custom_data], 
		@CREATED_DATETIME AS [created_datetime],
		@CREATED_BY AS [created_by],
		@JSON AS [json];

	RETURN SCOPE_IDENTITY();	
END
GO

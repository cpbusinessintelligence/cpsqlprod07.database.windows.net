SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO

CREATE PROCEDURE [dbo].[insert_nowgo_consignment_updates_failed_transactions]
(
	@json NVARCHAR(MAX) = NULL,
	@exception_stack_trace VARCHAR(500) = NULL
)
AS
BEGIN

    SET NOCOUNT ON

    INSERT INTO [dbo].failed_transactions
           ([json]
           ,[error_message]
           ,[is_processed]
           ,[created_datetime])
     VALUES
           (@json
           ,@exception_stack_trace
           ,0
           ,CONVERT(DATETIME, GETUTCDATE() AT TIME ZONE 'UTC' AT TIME ZONE 'AUS Eastern Standard Time'))
		
END
GO

SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[UFN_GET_INDEX_NAME] (@object_id int, @index_id tinyint)
RETURNS sysname
AS
BEGIN
    RETURN
	(
    SELECT name 
    FROM sys.indexes
    WHERE object_id = @object_id and index_id = @index_id
    )
END;
GO

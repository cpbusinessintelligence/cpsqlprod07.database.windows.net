SET ANSI_NULLS ON
SET QUOTED_IDENTIFIER ON
GO
CREATE FUNCTION [dbo].[udf_GetDayOfTheYearRange_FromDate] (@startDate DATETIME, @endDate DATETIME)
RETURNS @results TABLE (Number SMALLINT) AS
BEGIN

	DECLARE @partMin SMALLINT = 1
	DECLARE @partMax SMALLINT = 365

	DECLARE @expectedCount INT = DATEDIFF(DAY, @startDate, @endDate)

	IF (@expectedCount < 0)
		SET @expectedCount = @partMax + @expectedCount

	DECLARE @count INT = 0

	DECLARE @current SMALLINT = [dbo].[udf_GetDayOfTheYear](@startDate)

	WHILE (@count <= @expectedCount) BEGIN INSERT INTO @results (Number) VALUES (@current) IF (@current >= @partMax)
			SET @current = @partMin
		ELSE
			SET @current = @current + 1

		SET @count = @count + 1;

	END

	RETURN
END
GO
